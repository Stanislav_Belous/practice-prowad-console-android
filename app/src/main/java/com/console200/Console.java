package com.console200;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class Console extends Activity implements View.OnClickListener {
    Button channelUp;
    Button channelDown;
    Button volumeUp;
    Button volumeDown;
    Button arrowLeft;
    Button arrowRight;
    Button arrowTop;
    Button arrowBottom;
    Button btnOK;
    Button btnOff;
    Button btnMenu;

    Socket socket;
    OutputStream os;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.console);

        channelUp = (Button)findViewById(R.id.btnChUp);
        channelDown = (Button)findViewById(R.id.btnChDown);
        volumeUp = (Button)findViewById(R.id.btnVolumeUp);
        volumeDown = (Button)findViewById(R.id.btnVolumeDown);
        arrowLeft = (Button)findViewById(R.id.btnArrowLeft);
        arrowRight = (Button)findViewById(R.id.btnArrowRight);
        arrowTop = (Button)findViewById(R.id.btnArrowTop);
        arrowBottom = (Button)findViewById(R.id.btnArrowDown);
        btnOK = (Button)findViewById(R.id.btnOK);
        btnOff = (Button)findViewById(R.id.btnOff);
        btnMenu = (Button)findViewById(R.id.btnMenu);

        arrowLeft.setOnClickListener(this);
        arrowRight.setOnClickListener(this);
        arrowTop.setOnClickListener(this);
        arrowBottom.setOnClickListener(this);
        btnOK.setOnClickListener(this);
        channelUp.setOnClickListener(this);
        channelDown.setOnClickListener(this);
        volumeUp.setOnClickListener(this);
        volumeDown.setOnClickListener(this);
        btnOff.setOnClickListener(this);
        btnMenu.setOnClickListener(this);

        try {
            socket = new Socket("10.54.11.105", 2227);
            os = socket.getOutputStream();
        }
        catch (UnknownHostException e) {
            // TODO Auto-generated catch block
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    public byte[] message(int code) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream(8);
        try {
            stream.write(ByteBuffer.allocate(4).putInt(code).array());
            stream.write(new byte[]{1,2,3,4});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnChUp:
                try {
                    os.write(  this.message(2) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnChDown:
                try {
                    os.write(  this.message(3) );
                }
                catch (IOException ignored) {
                }
                break;
            case R.id.btnVolumeUp:
                try {
                    os.write(  this.message(4) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnVolumeDown:
                try {
                    os.write(  this.message(5) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnOK:
                try {
                    os.write(  this.message(6) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnArrowTop:
                try {
                    os.write(  this.message(7) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnArrowLeft:
                try {
                    os.write(  this.message(9) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnArrowDown:
                try {
                    os.write(  this.message(8) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnArrowRight:
                try {
                    os.write(  this.message(10) );
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnOff:
                try {
                    os.write(this.message(11));
                } catch (IOException ignored) {
                }
                break;
            case R.id.btnMenu:
                try {
                    os.write(this.message(11));
                } catch (IOException ignored) {
                }
                Intent menu = new Intent(getApplicationContext(), SystemMenu.class);
                startActivity(menu);
                break;
        }
    }
}

