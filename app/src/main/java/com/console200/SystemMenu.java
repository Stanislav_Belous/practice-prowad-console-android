package com.console200;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * Created by Администратор on 20.02.2015.
 */
public class SystemMenu extends Activity implements View.OnClickListener {

    Button plus;
    Button minus;
    Button top;
    Button right;
    Button down;
    Button left;
    Button cancel;

    int currentCode;
    Socket socket;
    OutputStream os;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.system_menu);

        plus = (Button)findViewById(R.id.plus);
        minus = (Button)findViewById(R.id.minus);
        top = (Button)findViewById(R.id.top);
        right = (Button)findViewById(R.id.right);
        down = (Button)findViewById(R.id.down);
        left = (Button)findViewById(R.id.left);
        cancel = (Button)findViewById(R.id.cancel);

        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        top.setOnClickListener(this);
        right.setOnClickListener(this);
        down.setOnClickListener(this);
        left.setOnClickListener(this);
        cancel.setOnClickListener(this);

        try {
            socket = new Socket( "10.54.11.105", 2227);
            os = socket.getOutputStream();
        }
        catch (UnknownHostException e) {
            // TODO Auto-generated catch block
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    public byte[] message(int code) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream(8);
        try {
            stream.write(ByteBuffer.allocate(4).putInt(code).array());
            stream.write(new byte[]{1,2,3,4});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();
    }

    @Override
    public void onClick(View v) {
        Intent menu;
        switch (v.getId()) {
            case R.id.cancel:
                try {
                    os.write(this.message(11));
                } catch (IOException ignored) {
                }
                menu = new Intent(getApplicationContext(), Console.class);
                startActivity(menu);
                break;
            case R.id.plus:
//                try {
//                    os.write(  this.message(2) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.minus:
//                try {
//                    os.write(  this.message(3) );
//                }
//                catch (IOException ignored) {
//                }
                break;
            case R.id.top:
//                try {
//                    os.write(  this.message(4) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.right:
                try {
                    os.write(  this.message(10) );
                } catch (IOException ignored) {
                }
                menu = new Intent(getApplicationContext(), PictureMenu.class);
                startActivity(menu);
                break;
            case R.id.down:
//                try {
//                    os.write(  this.message(6) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.left:
//                try {
//                    os.write(  this.message(7) );
//                } catch (IOException ignored) {
//                }
//                menu = new Intent(getApplicationContext(), VideoMenu.class);
//                startActivity(menu);
                break;
        }
    }
}
