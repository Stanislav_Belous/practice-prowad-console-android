package com.console200;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * Created by Администратор on 20.02.2015.
 */
public class VideoMenu extends Activity implements View.OnClickListener {

    Button plus;
    Button minus;
    Button top;
    Button right;
    Button down;
    Button left;
    Button cancel;

    int currentCode;
    Socket socket;
    OutputStream os;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_menu);

        plus = (Button)findViewById(R.id.plusV);
        minus = (Button)findViewById(R.id.minusV);
        top = (Button)findViewById(R.id.topV);
        right = (Button)findViewById(R.id.rightV);
        down = (Button)findViewById(R.id.downV);
        left = (Button)findViewById(R.id.leftV);
        cancel = (Button)findViewById(R.id.cancelV);

        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        top.setOnClickListener(this);
        right.setOnClickListener(this);
        down.setOnClickListener(this);
        left.setOnClickListener(this);
        cancel.setOnClickListener(this);

        try {
            socket = new Socket( "10.54.11.105", 2227);
            os = socket.getOutputStream();
        }
        catch (UnknownHostException e) {
            // TODO Auto-generated catch block
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    public byte[] message(int code) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream(8);
        try {
            stream.write(ByteBuffer.allocate(4).putInt(code).array());
            stream.write(new byte[]{1,2,3,4});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();
    }

    @Override
    public void onClick(View v) {
        Intent menu;
        switch (v.getId()) {
            case R.id.cancelV:
                try {
                    os.write(this.message(11));
                } catch (IOException ignored) {
                }
                menu = new Intent(getApplicationContext(), Console.class);
                startActivity(menu);
                break;
            case R.id.plusV:
//                try {
//                    os.write(  this.message(2) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.minusV:
//                try {
//                    os.write(  this.message(3) );
//                }
//                catch (IOException ignored) {
//                }
                break;
            case R.id.topV:
//                try {
//                    os.write(  this.message(4) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.rightV:
//                try {
//                    os.write(  this.message(5) );
//                } catch (IOException ignored) {
//                }
//                menu = new Intent(getApplicationContext(), SystemMenu.class);
//                startActivity(menu);
                break;
            case R.id.downV:
//                try {
//                    os.write(  this.message(6) );
//                } catch (IOException ignored) {
//                }
                break;
            case R.id.leftV:
                try {
                    os.write(  this.message(9) );
                } catch (IOException ignored) {
                }
                menu = new Intent(getApplicationContext(), PictureMenu.class);
                startActivity(menu);
                break;
        }
    }
}
